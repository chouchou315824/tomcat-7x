package com.luban;

import javax.servlet.ServletException;
import javax.servlet.SingleThreadModel;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Description
 * @Author houchenkai
 * @Created Date: 2021/1/9 16:50
 * @ClassName
 */
public class ServletDemo extends HttpServlet implements SingleThreadModel {

    // 当一个servlet实现了SingleThreadModel接口 每一个请求都对应一个单独Servlet实例
    // 没有实现时,默认所有请求都会共用一个Servlet实例

    //  因为一个应用下会有多个Servlet,而一个Servlet类有可能(implements SingleThreadModel)会对应多个Servlet实例

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

}
